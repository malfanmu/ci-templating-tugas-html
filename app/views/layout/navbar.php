<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url()."assets/css/style.css"?>">
	<title>Navbar</title>
</head>
<body>
	<!-- NAVBAR -->
	<div class="nav">
		<div class="up">
			<div class="logo">
				<a href="home.html">
					<img src="<?php echo base_url('assets\img\logo.png') ?>">
				</a>
			</div>
		</div>
		<div class="mid">
			<h1>WDYMEAN</h1>
			<ul>
				<li>
					<a href="">HOME</a>
				</li>
				<li>
					<a href="">ABOUT</a>
				</li>
				<li>
					<a href="<?php echo site_url('welcome') ?>">ARTICLE</a>
				</li>
				<li>
					<a href="<?php echo site_url('welcome/login') ?>">LOG IN</a>
				</li>
			</ul>
		</div>
		<div class="up"></div>
	</div>
	<div class="follow">
		<a href="">
			<img src="<?php echo base_url('assets\img\fb.png') ?>" title="Facebook">
		</a>
		<a href="">
			<img src="<?php echo base_url('assets\img\yt.png') ?>" title="Youtube">
		</a>
		<a href="">
			<img src="<?php echo base_url('assets\img\ig.png') ?>" title="Instagram">
		</a>
	</div>
</body>
</html>