<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="<?= base_url('assets\img\logo.png') ?>" rel="shortcut icon">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
	<title>Dashboard</title>
</head>
<body>
	<div class="dasup">
		<div class="bung">
			<div class="dul">
				<img src="<?= base_url('assets\img\logo.png')?>">
			</div>
			<div class="dil">
				<h1>WDYMEAN</h1>
			</div>
		</div>
		<div class="profil">
			<div class="logout">
				<p><a href="<?= site_url('welcome\login')?>">Log Out</a></p>
			</div>
			<div class="icon">
				<div class="nama">
					<h3>M Alfan Miftachul U</h3>
				</div>
				<img src="<?= base_url('assets\img\profil.png')?>">
			</div>
		</div>
	</div>
	<div class="dasleft">
		<h1>
			<img src="<?= base_url('assets\img\das.png')?>">
			<span> D</span>ashboard
		</h1>
		<hr width="60%" color="black">
		<h1>
			<img src="<?= base_url('assets\img\prof.png')?>">
			<span>U</span>ser <span>P</span>rofile
		</h1>
		<hr width="60%" color="black">
		<h1>
			<img src="<?= base_url('assets\img\artikel.png')?>">
			L<span>ist</span> A<span>rticle</span>
		</h1>
		<hr width="100%" color="#bd1e1e">
		<h1>
			<img src="<?= base_url('assets\img\browser.png')?>">
			<span>N</span>otification
		</h1>
		<hr width="60%" color="black">
		<h1>
			<img src="<?= base_url('assets\img\add.png')?>">
			<a href="<?= site_url('welcome/dasadd') ?>"><span>A</span>dd <span>A</span>rticle</a>
		</h1>
		<hr width="60%" color="black">
		<div class="copy">
			<h3>All right Reserved.</h3>
			<h4>Copyright. 2018</h4>
		</div>
	</div>
	<div class="dasright">
		<h2>List Article</h2>
		<table>
			<tr>
				<th width="50px">No</th>
				<th width="650px">Title</th>
				<th colspan="2">Action</th>
			</tr>
			<tr>
				<th>1.</th>
				<th><a href="<?= site_url('welcome/jon') ?>">Jonatan Christie dan Sisi Lain Euforia Kemenangan di Asian Games 2018</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>2.</th>
				<th><a href="">Inilah 10 Skill Paling Dibutuhkan di Tahun 2020, Kamu Sudah Memilikinya?</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>3.</th>
				<th><a href="">5 Budaya Indonesia yang Terkenal di Dunia</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>4.</th>
				<th><a href="">5 Alasan Kenapa Sopir Truk Bikin Kalimat Nyentrik di Belakang Truknya</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>5.</th>
				<th><a href="">Perkembangan Street Art di Indonesia, Karya Seniman Jalanan yang Menghidupkan Kota</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>6.</th>
				<th><a href="">Bukan Horor, Inilah Makna Lagu Lingsir Wengi yang Sebenarnya</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>7.</th>
				<th><a href="">Jalan-jalan ke Pasar Papringan, Ada Pasar di Tengah Hutan Bambu</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>8.</th>
				<th><a href="">3 Pantai Cantik dengan Ayunan di Tengah Laut yang Wajib Kamu Kunjungi</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>7.</th>
				<th><a href="">Tradisi Mudik di Indonesia dalam Perspektif Budaya</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>9.</th>
				<th><a href="">Bangga! PBB Beri Penghargaan Global Green City untuk Surabaya</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>
			<tr>
				<th>10.</th>
				<th><a href="">Ingin Eksis di Media Sosial? Nanti Dulu, Hati-hati ‘Wabah’ FOMO</a></th>
				<th class="ed it">Edit</th>
				<th class="ed del">Delete</th>
			</tr>

		</table>
	</div>
</body>
</html>