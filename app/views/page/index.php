<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="<?= base_url('assets\img\logo.png') ?>" rel="shortcut icon">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
	<title>Home</title>
</head>
<body>
	<!-- NAVBAR -->
	<?php $this->load->view('layout/navbar'); ?>
	<!-- END NAVBAR -->
	
	<!-- ARTIKEL -->
	<div class="sticky">
		<h1>Jonatan Christie dan Sisi Lain Euforia Kemenangan di Asian Games 2018</h1>
	</div>
	<div class="content">
		<!-- <div class="sub left" style="background-image: url(content/img/jon.jpg);"> -->
		<div class="sub left">
			<img src="<?= base_url('assets/img/jon.jpg')?>">
			<div class="titel">
				<h1>Jonatan Christie....</h1>
			</div>
		</div>
		<div class="sub right">
			<div class="cuplikan">
				<p>Pada pertandingan final di Istora Senayan, hari Selasa (28/8/2018), Indonesia menyumbangkan medali emas ke-23 melalui cabang olahraga bulutangkis  tunggal putra. Dialah Jonatan Christie, pebulutangkis 20 tahun yang cukup menyita perhatian masyarakat Indonesia di perhelatan Asian Games tahun ini.</p>
			</div>
			<div class="cuplikan1">
				<div class="author">
					<p>Penulis: M Alfan MU&emsp;Kategori: Berita</p>
				</div>
				<div class="read">
					<a href="<?= site_url('welcome/jon') ?>">Read More</a>
				</div>
			</div>
		</div>
	</div>

	<!-- ARTIKEL -->
	<div class="sticky">
		<h1>Inilah 10 Skill Paling Dibutuhkan di Tahun 2020, Kamu Sudah Memilikinya?</h1>
	</div>
	<div class="content">
		<!-- <div class="sub left" style="background-image: url(content/img/skil.jpg);"> -->
		<div class="sub left" style="background-image: url(content/img/skil.jpg);">
			<img src="<?= base_url('assets/img/skil.jpg')?>">
			<div class="titel">
				<h1>Inilah 10 Skill....</h1>
			</div>
		</div>
		<div class="sub right">
			<div class="cuplikan">
				<p>Di sela-sela kesibukan sehari-hari, pernahkah kamu berpikir tentang skill yang paling dibutuhkan dunia? Atau hal-hal baru yang harus dilakukan untuk membuatmu lebih unggul dalam hal yang ditekuni saat ini? Atau mungkin sudah mempersiapkan diri untuk tantangan hidup selanjutnya?</p>
			</div>
			<div class="cuplikan1">
				<div class="author">
					<p>Penulis: M Alfan MU&emsp;Kategori: Inspirasi</p>
				</div>
				<div class="read">
					<a href="">Read More</a>
				</div>
			</div>
		</div>
	</div>

	<!-- ARTIKEL -->
	<div class="sticky">
		<h1>5 Budaya Indonesia yang Terkenal di Dunia</h1>
	</div>
	<div class="content">
		<!-- <div class="sub left" style="background-image: url(content/img/kecak.jpg);"> -->
		<div class="sub left" style="background-image: url(content/img/kecak.jpg);">
			<img src="<?= base_url('assets/img/kecak.jpg')?>">
			<div class="titel">
				<h1>5 Budaya Indonesia....</h1>
			</div>
		</div>
		<div class="sub right">
			<div class="cuplikan">
				<p>Bicara <strong>budaya Indonesia</strong>, berarti kita bicara tentang sebuah kekayaan sebuah bangsa yang tak ternilai harganya. Sebagai penerus bangsa sudah menjadi kewajiban kita untuk dapat terus menjaga dan melestarikan budaya Indonesia sebagai warisan leluhur.</p>
			</div>
			<div class="cuplikan1">
				<div class="author">
					<p>Penulis: M Alfan MU&emsp;Kategori: Budaya</p>
				</div>
				<div class="read">
					<a href="">Read More</a>
				</div>
			</div>
		</div>
	</div>

	<!-- FOOTER -->
	<?php $this->load->view('layout/footer'); ?>
	<!-- END FOOTER -->
</body>
</html>