<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="<?= base_url('assets\img\logo.png') ?>" rel="shortcut icon">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/login.css')?>">
	<title>Login Page</title>
</head>
<body>
	<div class="sign up">
		<div class="box">
			<form action="<?php echo site_url('welcome/daslist') ?>">
				<div class="input">
					<h3>Username</h3>
					<input type="text" placeholder="Username" required>
					<h3>E-mail Address</h3>
					<input type="text" placeholder="E-mail Address" required>
					<h3>New Password</h3>
					<input type="password" placeholder="Password" required>
				</div>
				<div class="button">
					<button type="submit">
						Sign Up
					</button>
				</div>
			</form>
		</div>
		<p>Already Have an Account ??</p>
		<h5>scroll down</h5>
	</div>

	<div class="sign in">
		<div class="box">
			<form action="daslist.html">
				<div class="input">
					<h3>Username</h3>
					<input type="text" placeholder="Username" required>
					<h3>Password</h3>
					<input type="password" placeholder="Password" required>
					<h4>
						<a href="#">Forgot Password?</a>
					</h4>
				</div>
				<div class="button">
					<button type="submit">
						Sign In
					</button>
				</div>
			</form>
		</div>
		<p>Already Have an Account ??</p>
		<h5>scroll down</h5>
	</div>
</body>
</html>