<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="<?= base_url('assets\img\logo.png') ?>" rel="shortcut icon">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
	<title>Add Article</title>
</head>
<body>
	<div class="dasup">
		<div class="bung">
			<div class="dul">
				<img src="<?= base_url('assets\img\logo.png')?>">
			</div>
			<div class="dil">
				<h1>WDYMEAN</h1>
			</div>
		</div>
		<div class="profil">
			<div class="logout">
				<p><a href="<?= site_url('welcome\login')?>">Log Out</a></p>
			</div>
			<div class="icon">
				<div class="nama">
					<h3>M Alfan Miftachul U</h3>
				</div>
				<img src="<?= base_url('assets\img\profil.png')?>">
			</div>
		</div>
	</div>
	<div class="dasleft">
		<h1>
			<img src="<?= base_url('assets\img\das.png')?>">
			<span> D</span>ashboard
		</h1>
		<hr width="60%" color="black">
		<h1>
			<img src="<?= base_url('assets\img\prof.png')?>">
			<span>U</span>ser <span>P</span>rofile
		</h1>
		<hr width="60%" color="black">
		<h1>
			<img src="<?= base_url('assets\img\artikel.png')?>">
			<a href="<?= site_url('welcome/daslist') ?>"><span>L</span>ist <span>A</span>rticle</a>
		</h1>
		<hr width="60%" color="black">
		<h1>
			<img src="<?= base_url('assets\img\browser.png')?>">
			<span>N</span>otification
		</h1>
		<hr width="60%" color="black">
		<h1>
			<img src="<?= base_url('assets\img\add.png')?>">
			A<span>dd</span> A<span>rticle</span>
		</h1>
		<hr width="100%" color="#bd1e1e">
		<div class="copy">
			<h3>All right Reserved.</h3>
			<h4>Copyright. 2018</h4>
		</div>
	</div>
	<div class="dasright">
		<h2>Article Title :</h2>
		<center><hr width="15%" color="black"></center>
		<input class="art tit" type="text" placeholder="Input Title of Article You Want to Make">

		<h2>Article</h2>
		<center><hr width="15%" color="black"></center>
		<textarea class="art isi" cols="200" rows="10" placeholder="The Words Are The Bigger Thing In The Wold..."></textarea>
		
		<p>Category :</p>
		<div class="artcat">
			<p>news<b> | X</b></p>
			<p>inspiration<b> | X</b></p>
			<p>unique<b> | X</b></p>
			<p>Technology<b> | X</b></p>
			<p>culture<b> | X</b></p>
			<p>vacation<b> | X</b></p>
			<p>tutorial<b> | X</b></p>
			<p>urban<b> | X</b></p>
			<p>tips & tricks<b> | X</b></p>
			<p>education<b> | X</b></p>
		</div>
		<div class="but">
			<button type="submit">Post Now</button>
		</div>
	</div>
</body>
</html>