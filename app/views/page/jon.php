<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Jonathan</title>
</head>
<body>
	<!-- NAVBAR -->
	<?php $this->load->view('layout/navbar'); ?>
	<!-- END NAVBAR -->

		<!-- DETAIL ARTIKEL -->
		<!-- JUDUL -->
	<div class="detjud">
		<h1>Jonatan Christie dan Sisi Lain Euforia Kemenangan di Asian Games 2018</h1>
		<center><hr width="50%" color="darkslategray"></center>
		<p>Home<b> / </b>Article<b> / </b>Category<b> / </b>News<b> / </b>Jonatan Christie dan Sisi Lain</p>
	</div>

		<!-- DETAIL ARTIKEL LEFT -->
	<div class="detleft">
		<div class="subdet">
			<!-- <div class="aa" style="background-image: url(img/jon.jpg);"></div> -->
			<div class="aa">
				<img src="<?= base_url('assets/img/jon.jpg')?>">
			</div>
			<div class="bb">
				<a href="jon.html"><p>Jonatan Christie dan Sisi Lain Euforia Kemenangan di Asian Games 2018</p></a>
			</div>
		</div>
		<center><hr width="60%" color="black"></center>
		<div class="subdet">
			<!-- <div class="aa" style="background-image: url(img/kecak.jpg);"></div> -->
			<div class="aa">
				<img src="<?= base_url('assets/img/kecak.jpg')?>">
			</div>
			<div class="bb">
				<a href="kecak.html"><p>5 Budaya Indonesia yang Terkenal di Dunia</p></a>
			</div>
		</div>
		<center><hr width="100%" color="black"></center>
		<div class="subdet">
			<!-- <div class="aa" style="background-image: url(img/skil.jpg);"></div> -->
			<div class="aa">
				<img src="<?= base_url('assets/img/skil.jpg')?>">
			</div>
			<div class="bb">
				<a href="skil.html"><p>Inilah 10 Skill Paling Dibutuhkan di Tahun 2020, Kamu Sudah Memilikinya?</p></a>
			</div>
		</div>
		<center><hr width="60%" color="black"></center>
		<div class="subdet">
			<!-- <div class="aa" style="background-image: url(img/sopir.jpg);"></div> -->
			<div class="aa">
				<img src="<?= base_url('assets/img/sopir.jpg')?>">
			</div>
			<div class="bb">
				<a href="jon.html"><p>5 Alasan Kenapa Sopir Truk Bikin Kalimat Nyentrik di Belakang Truknya</p></a>
			</div>
		</div>
		<center><hr width="100%" color="black"></center>
	</div>
	<div class="detright">
		<!-- <div class="img" style="background-image: url(img/jon.jpg);"> -->
		<div class="img">
			<img src="<?= base_url('assets/img/jon.jpg')?>">
			<div class="timpa">
				<h1>Jonatan Christie</h1>
				<p>Writter <b>M Alfan MU</b> - Posted <b>2 Juli 2018</b></p>
			</div>
		</div>
		<br><br>
		<p>
			Pada pertandingan final di Istora Senayan, hari Selasa (28/8/2018), Indonesia menyumbangkan medali emas ke-23 melalui cabang olahraga bulutangkis  tunggal putra. Dialah Jonatan Christie, pebulutangkis 20 tahun yang cukup menyita perhatian masyarakat Indonesia di perhelatan Asian Games tahun ini.
		</p>
		<p>
			Jojo, panggilan akrabnya, berhak berbangga atas kemenangannya karena ia membuat tunggal putra Indonesia berhasil mendapatkan medali emas kembali sejak 2006 yang waktu itu diraih oleh Taufik Hidayat pada Asian Games 2006 di Doha, Qatar. Atlet asal Jakarta itu menyumbangkan medali emas bagi Indonesia setelah berhasil mengalahkan Chou Tien Chen asal Taiwan 21-18, 20-22, 15-22.
		</p>
		<center>
			<img src="<?= base_url('assets\img\jon1.jpg') ?>">
		</center><br>
		<p>
			Tapi, di sini kita tidak sedang membahas kemenangan dalam bentuk angka-angka. Yang pasti, keberhasilan Jojo kali ini membuat seluruh pecinta bulutangkis di Indonesia sangat bergembira. Bahkan masyarakat yang sebelumnya kurang berminat pada bulutangkis pun ikut larut dalam euforia kemenangan. Kalau kamu sempat menyaksikan penampilannya di Asian Games 2018, pasti paham alasannya.
		</p>
		<p>
			Selain lihai dalam bermain, performa Jojo juga didukung oleh wajah tampan dan tubuh six pack yang membuat para wanita mendadak histeris melihatnya. Apalagi selebrasi buka baju yang dilakukannya, walau entah apa tujuannya, tapi pesona Jonatan Christie seolah menghipnotis kaum hawa dari berbagai usia. Mulai dari anak remaja sampai mama muda mendadak kena ‘demam’ Jojo. Terlepas dari reaksi netizen yang begitu heboh, tampaknya kita perlu mengapresiasi pernyataan Jojo sendiri saat diwawancarai usai pertandingan “…ini sudah berakhir, tinggal naik podium, dan setelah itu saya bukan juara lagi. Kita harus mulai dari awal lagi, saya dan tim harus bersiap lagi.” Terlihat sekali bagaimana mental juara dalam dirinya.
		</p>
		<p>
			Hmm, tapi kira-kira sampai kapan euforia kemenangan seperti ini berlangsung? Tentunya tak akan selamanya. Tidak jauh beda dengan hal-hal yang viral karena dukungan media sosial. Seseorang atau kelompok tertentu melambung namanya karena prestasinya, orang-orang lain menyaksikan, mengagumi, atau sekadar ikut berkomentar. Coba kita lihat para pemirsa Asian Games yang mengelu-elukan prestasi para atlet yang tampil gemilang. Tidak hanya bulutangkis, tapi juga cabang olahraga lainnya. Hanya dengan melihat mereka berdiri di podium, rasa-rasanya jiwa nasionalisme meningkat beberapa derajat. Apakah kamu juga merasakannya?
		</p>
		<center>
			<img src="<?= base_url('assets\img\jon2.jpg') ?>">
		</center><br>
		<p>
			Kembali ke Jojo dan keberhasilannya meraih kemenangan. Apakah semua yang menyaksikan jalannya pertandingan itu memang karena mengagumi skill bermainnya? Oh tentu tidak. Walau memang ada penonton yang jeli menganalisis pertandingan, di saat yang lain hanya menikmati sesuatu yang sedang trending dan menjelma jadi sosok selebriti yang populer di bidangnya.
		</p>
		<h1>
			<b>Parasocial Relationship</b>
		</h1>
		<p>
			Coba ingat-ingat saat kamu merasa dekat dengan public figure yang menjadi idola. Meskipun kamu tidak mengenalnya secara langsung. Lalu pernahkah kamu merasakan bahwa public figure yang begitu dekat itu tidak mudah terjangkau? Meskipun tinggal di kota yang sama, tapi tidak mudah untuk bertemu mereka. Jika dipandang dari aspek hubungan antara public figure dan non-public figure, maka hubungan semacam itu disebut sebagai hubungan parasosial (parasocial relationship) yang pada intinya merupakan hubungan satu arah.
		</p>
		<p>
			Hubungan parasosial (parasocial relationship) adalah istilah yang digunakan oleh ilmuwan sosial tentang hubungan interpersonal di mana satu pihak tahu banyak tentang yang lain, tapi pihak lain tidak. Bentuk yang umum dari hubungan satu sisi seperti ini adalah hubungan antara selebriti dengan penggemar. Hubungan parasosial pertama kali dikembangkan oleh Donald Horton dan Richard Wohl di tahun 1956. Mereka menyebutkan bahwa komunikasi antara masyarakat biasa dengan public figure itu bersifat satu arah. Hal tersebut disebabkan karena perilaku orang terkenal bisa diamati dan ‘dikonsumsi’ oleh masyarakat karena bantuan media. Sebaliknya, perilaku masyarakat sebagai pengguna media tidak bisa diamati oleh si public figure yang diidolakan itu.
		</p>
		<p>
			Tak jarang bahwa seorang penggemar membuat semacam ilusi tentang seseorang yang dikaguminya di televisi, sehingga terciptalah sebuah hubungan satu arah. Tokoh selebritis itu dikenal, dikagumi, diikuti, padahal mereka sama sekali tidak mengenal penggemarnya itu.
		</p>
		<p>
			Bisa dibilang kalau hubungan seperti ini adalah sebuah ilusi mengenai hubungan langsung antara masyarakat umum sebagai pemirsa dengan artis atau bahkan atlet yang menampilkan kebolehannya. Yang jelas, hubungan satu arah di mana penonton bisa merasa memiliki hubungan dengan sosok idolanya merupakan hasil pencitraan yang dibangun oleh media. Terlepas dari jenis hubungan sau arah itu, media pun butuh sosok populer untuk diberitakan agar menarik perhatian khalayak.
		</p>
		<p>
			Lalu faktor apa yang menjadi penyebab munculnya hubungan parasosial? Yang paling dominan menjadi latar belakang hubungan parasosial adalah karena kesamaan antara individu dengan idolanya, entah itu dalam hal kepribadian, tingkah laku, selera, penampilan, pengalaman, latar belakang sosial, value kehidupan yang diyakini, dan kesamaan dalam hal lainnya. Umumnya, seseorang akan lebih tertarik pada orang yang memiliki value yang sama dengan dirinya. Atau kalaupun tidak sama, kebanyakan orang akan mudah mengagumi orang-orang berprestasi.
		</p>
		<p>
			Ada yang unik dari jenis interaksi seperti ini. Bahkan mereka bisa lebih mengenal sosok idolanya daripada orang di sekelilingnya. Jenis hubungan seperti ini tentu memiliki efek tersendiri yang namanya pseudo friendship di mana penggemar merasa memiliki hubungan persahabatan dengan sosok idolanya. Tentu saja persahabatan seperti ini tidak membutuhkan komitmen dengan idolanya.
		</p>
		<p>
			Para pakar di bidangnya menyebut bahwa keadaan seperti ini lebih sering terjadi pada perempuan karena mereka fleksibel dalam memilih sosok yang diidolakan. Mereka bisa mengidolakan laki-laki karena penampilan fisiknya, tapi juga bisa mengidolakan sesama perempuan karena ingin mencontohnya dalam penampilan, sikap, maupun belajar skill yang sama dengannya.
		</p>
		<center>
			<img src="<?= base_url('assets\img\jon3.jpg') ?>">
		</center><br>
		<p>
			Demikianlah kiranya sisi lain euforia kemenangan di Asian Games 2018 dilihat dari bagaimana penonton mengidolakan sang juara. Sekali lagi selamat untuk para pemenang yang telah mengarumkan nama bangsa. Setidaknya momen-momen seperti inilah yang bisa mempersatukan berbagai kalangan, baik di dunia nyata maupun di social media. Satu lagi, bicara soal social media tentu kurang lengkap kalau tidak mengamati kelakuan netizen. Sejumlah akun populer di Instagram yang mengunggah foto-foto atlet berprestasi, (tentu saja) langsung banjir ucapan selamat. Bukan hanya ucapan selamat dan kebanggaan, ada yang menggelikan pada setiap foto-foto kemenangan Jojo. Baik di akun Instagram pribadi maupun akun-akun berita. Bisa ditebak, kebanyakan komentar dari kaum hawa, seperti ini misalnya (tanpa perlu menyebut akunnya).
		</p>
		<hr width="100%" color="darkslategray" size="10px">

		<!-- COMENT -->
		<h1>Comment</h1>
		<ul>
			<hr width="100%" color="darkslategray">
			<li><b>| Eko Patrio →</b></li>
			<li>Idolanya Akoh.. Pengen Ketemu Langsung dan Foto Bareeng.</li>
			<li>- 24 Agustus 2018 - <b>Reply</b></li>
					<ul>
						<li><b>| Ning Karmila →</b></li>
						<li><b>@Eko</b> Plis deh Ga Usah Lebay Gitu haha</li>
						<li>- 30 Agustus 2018 - <b>Reply</b></li>
					</ul><br>
			<hr width="100%" color="darkslategray">

			<li><b>| Samuel Gezwin →</b></li>
			<li>Badanya Keker Banget Anjiirr, Jadi Pengen wkwk</li>
			<li>- 3 September 2018 - <b>Reply</b></li><br>
			<hr width="100%" color="darkslategray">
			
			<li><b>| Siti Anabella →</b></li>
			<li>Eh BTW Gue Juga Mau ah Jadi Pacar Dia, Walaupun Yang Kedua</li>
			<li>- 10 September 2018 - <b>Reply</b></li>
					<ul>
						<li><b>| Fikri →</b></li>
						<li>Woy Pakek Otak, Kemana Harga Diri Lo.. dih Gampangan</li>
						<li>- 11 September 2018 - <b>Reply</b></li>

						<li><b>| Isabel →</b></li>
						<li>Sabar Bang, Sabar Anggep Aja Ini Ujian Dari Tuhan hahaha</li>
						<li>- 20 September 2018 - <b>Reply</b></li><br>
					</ul>
			<hr width="100%" color="darkslategray">

			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>

	<!-- FOOTER -->
	<?php $this->load->view('layout/footer'); ?>
	<!-- END FOOTER -->
</body>
</html>