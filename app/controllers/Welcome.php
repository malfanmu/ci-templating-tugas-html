<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('page/index');
	}

	public function login()
	{
		$this->load->view('page/login');
	}

	public function daslist()
	{
		$this->load->view('page/daslist');
	}

	public function dasadd()
	{
		$this->load->view('page/dasadd');
	}

	public function jon()
	{
		$this->load->view('page/jon');
	}
}
